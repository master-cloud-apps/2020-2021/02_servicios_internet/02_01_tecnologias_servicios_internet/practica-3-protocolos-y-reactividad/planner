package org.eyo.planner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eyo.planner.model.TopographicDetailsDTO;
import org.eyo.planner.repository.TopoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RestClientTest(TopoService.class)
@EnableAsync
class TopoServiceTest {

    @Autowired
    private TopoService topoService;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void givenCityNameMadrid_whenCallGetTopo_shouldReturnLandscape() throws ExecutionException, InterruptedException,
            JsonProcessingException {
        String detailsString =
                objectMapper.writeValueAsString(new TopographicDetailsDTO("Alava", "mountain"));
        this.server.expect(requestTo("http://localhost:8080/api/topographicdetails/Alava"))
                .andRespond(withSuccess(detailsString, MediaType.APPLICATION_JSON));

        CompletableFuture<TopographicDetailsDTO> madridTopo = this.topoService.getTopoFromCityName("Alava");

        TopographicDetailsDTO topo = madridTopo.get();

        assertEquals("Alava", topo.getId());
        assertEquals("mountain", topo.getLandscape());
    }

    @Test
    void whenCallGetTopoCityNotFound_shouldReturnFlatCity() throws ExecutionException, InterruptedException {
        this.server.expect(requestTo("http://localhost:8080/api/topographicdetails/MexicoDF"))
                .andRespond(withSuccess("", MediaType.APPLICATION_JSON));

        TopographicDetailsDTO mexicoTopo = this.topoService.getTopoFromCityName("MexicoDF").get();

        assertEquals("MexicoDF", mexicoTopo.getId());
        assertEquals("flat", mexicoTopo.getLandscape());
    }
}
